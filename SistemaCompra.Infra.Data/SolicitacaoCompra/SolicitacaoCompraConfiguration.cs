﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore;
using SolicitacaoCompraAgg = SistemaCompra.Domain.SolicitacaoCompraAggregate;

namespace SistemaCompra.Infra.Data.SolicitacaoCompra
{
    public class SolicitacaoCompraConfiguration : IEntityTypeConfiguration<SolicitacaoCompraAgg.SolicitacaoCompra>
    {
        public void Configure(EntityTypeBuilder<SolicitacaoCompraAgg.SolicitacaoCompra> builder)
        {
            builder.ToTable("SolicitacaoCompra");
            builder.HasKey(x => x.Id);
            builder.OwnsOne(c => c.TotalGeral, b => b.Property("Value").HasColumnType("decimal(10,2)").HasColumnName("TotalGeral"));
            builder.OwnsOne(c => c.CondicaoPagamento, b => b.Property("Valor").HasColumnType("int").HasColumnName("CondicaoPagamento"));
            builder.OwnsOne(c => c.NomeFornecedor, b => b.Property("Nome").HasColumnType("varchar(250)").HasColumnName("NomeFornecedor"));
            builder.OwnsOne(c => c.UsuarioSolicitante, b => b.Property("Nome").HasColumnType("varchar(250)").HasColumnName("UsuarioSolicitante"));
        }
    }
}
