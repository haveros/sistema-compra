﻿using SistemaCompra.Domain.Core;
using SistemaCompra.Domain.Core.Model;
using SistemaCompra.Domain.ProdutoAggregate;
using SistemaCompra.Domain.SolicitacaoCompraAggregate.Events;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SistemaCompra.Domain.SolicitacaoCompraAggregate
{
    public class SolicitacaoCompra : Entity
    {
        public UsuarioSolicitante UsuarioSolicitante { get; private set; }
        public NomeFornecedor NomeFornecedor { get; private set; }
        public IList<Item> Itens { get; private set; }
        public DateTime Data { get; private set; }
        public Money TotalGeral { get; private set; }
        public Situacao Situacao { get; private set; }
        public CondicaoPagamento CondicaoPagamento { get; set; }

        private SolicitacaoCompra() { }

        public SolicitacaoCompra(string usuarioSolicitante, string nomeFornecedor)
        {
            Id = Guid.NewGuid();
            UsuarioSolicitante = new UsuarioSolicitante(usuarioSolicitante);
            NomeFornecedor = new NomeFornecedor(nomeFornecedor);
            Itens = new List<Item>();
            Data = DateTime.Now;
            Situacao = Situacao.Solicitado;
        }

        public void AdicionarItem(Produto produto, int qtde)
        {
            if (produto == null || produto.Id == Guid.Empty)
                throw new BusinessRuleException("Produto não encontrado");

            Itens.Add(new Item(produto, qtde));
        }

        public void RegistrarTotalCompra(IEnumerable<Item> itens)
        {
            if (!itens.Any() || itens.Sum(i => i.Qtde) < 1)
                throw new BusinessRuleException("A solicitação de compra deve possuir itens!");

            TotalGeral = new Money(itens.Sum(x => x.Subtotal.Value));

            if (TotalGeral.Value > 50000)
                CondicaoPagamento = new CondicaoPagamento(30);
            else
                CondicaoPagamento = new CondicaoPagamento(0);

        }
    }
}
