﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SistemaCompra.Application.SolicitacaoCompra.Models
{
    public class ItemModel
    {
        public Guid Id { get; set; }
        public int Quantidade { get; set; }
    }
}
