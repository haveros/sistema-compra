﻿using MediatR;
using SistemaCompra.Application.SolicitacaoCompra.Command.RegistrarCompra;
using SistemaCompra.Infra.Data.UoW;
using System.Threading;
using System.Threading.Tasks;
using SolicitacaoCompraAgg = SistemaCompra.Domain.SolicitacaoCompraAggregate;
using ProdutoAgg = SistemaCompra.Domain.ProdutoAggregate;

namespace SistemaCompra.Application.SolicitacaoCompra.Command.RegistrarSolicitacaoCompra
{
    public class RegistrarCompraCommandHandler : CommandHandler, IRequestHandler<RegistrarCompraCommand, bool>
    {
        private readonly SolicitacaoCompraAgg.ISolicitacaoCompraRepository SolicitacaoCompraRepository;
        private readonly ProdutoAgg.IProdutoRepository ProdutoAgg;
        public RegistrarCompraCommandHandler(SolicitacaoCompraAgg.ISolicitacaoCompraRepository SolicitacaoCompraRepository, ProdutoAgg.IProdutoRepository ProdutoAgg, IUnitOfWork uow, IMediator mediator) : base(uow, mediator)
        {
            this.SolicitacaoCompraRepository = SolicitacaoCompraRepository;
            this.ProdutoAgg = ProdutoAgg;
        }

        public Task<bool> Handle(RegistrarCompraCommand request, CancellationToken cancellationToken)
        {
            var SolicitacaoCompra = new SolicitacaoCompraAgg.SolicitacaoCompra(request.UsuarioSolicitante, request.NomeFornecedor);

            foreach(var item in request.Itens)
            {
                var produto = ProdutoAgg.Obter(item.Id);
                SolicitacaoCompra.AdicionarItem(produto, item.Quantidade);
            }

            SolicitacaoCompra.RegistrarTotalCompra(SolicitacaoCompra.Itens);

            SolicitacaoCompraRepository.Registrar(SolicitacaoCompra);

            Commit();
            PublishEvents(SolicitacaoCompra.Events);

            return Task.FromResult(true);
        }
    }
}
